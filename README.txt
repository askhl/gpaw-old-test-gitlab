--------------------------------------
GPAW: a grid-based real-space PAW code
--------------------------------------

Copyright (c) 2004 CAMP



Installation
------------

For installation instructions, please see:

  http://wiki.fysik.dtu.dk/gpaw



License
-------

See LICENSE



---------------  -------------------------------
File             Description
---------------  -------------------------------
README.txt       This file
LICENSE          GPL-license summary
COPYING          GPL-license
setup.py         distutils script
config.py        Configuration stuff
customize.py     Help distutils find libraries
MANIFEST.in      distutils MANIFEST.in file
---------------  -------------------------------

---------  ---------------
Directory  Description
---------  ---------------
c          C-extensions
doc        Documentation 
gpaw       The Python code
gpaw/test  Test suite
tools      Useful tools
---------  ---------------
